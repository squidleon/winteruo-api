package org.bitbucket.winteruo.api.messages.enums;

public enum Notoriety {

	NONE(0),
	INNOCENT(1),
	GUILD(2),
	NEUTRAL(3),
	CRIMINAL(4),
	ENEMY(5),
	MURDERER(6),
	INVURNERABLE(7);
	
	private final int value;
	
	private Notoriety(int value) {
	
		this.value = value;
	}
	
}
