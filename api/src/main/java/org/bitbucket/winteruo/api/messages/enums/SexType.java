package org.bitbucket.winteruo.api.messages.enums;

public enum SexType {

	MALE_HUMAN_1(0),
	FEMALE_HUMAN_1 (1),
	MALE_HUMAN_2(2),
	FEMALE_HUMAN_2(3),
	MALE_ELF(4),
	FEMALE_ELF(5),
	MALE_GARGOYLE(6),
	FEMALE_GARGOYLE(7);

	private final int value;

	private SexType(int value) {
		this.value = value;
	}
	
	public static SexType getValue(int value)
	{
		for (int i=0;i<values().length;i++)
		{
			if (values()[i].value == value)
				return values()[i];
		}
		
		return null;
	}

}
