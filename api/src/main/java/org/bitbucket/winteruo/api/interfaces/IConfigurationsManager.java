package org.bitbucket.winteruo.api.interfaces;

public interface IConfigurationsManager {

	/***
	 * Store object to disk
	 * @param classz
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public abstract boolean saveConfiguration(Class<?> classz, Object data);

	/**
	 * Load default configuration passing class
	 * @param classz
	 * @return
	 * @throws Exception
	 */
	public abstract <E> Object loadConfiguration(Class<?> classz);
	/**
	 * Retrive Root directory
	 * @return
	 */
	public String getRootDirectory();
	
	
	public boolean saveBinaryConfiguration(Class<?> classz, Object object);
	public <E> Object loadBinaryConfiguration(Class<?> classz);
}