package org.bitbucket.winteruo.api.model;

public interface Item extends JUoEntity {
	void accept(ItemVisitor itemManager);
}
