package org.bitbucket.winteruo.api.utils;

public class PacketReader {

	
	private byte[] m_Data;
	private int m_Size;
	int m_Index = 1;
	
	public byte[] getBytes()
	{
		return m_Data;
	}
	
	public PacketReader(byte[] buffer, int size) {
	   this.m_Data = buffer;
	   this.m_Size = size;
	}
	
	public int ReadInt32()
	{
		if ( (m_Index + 4) > m_Size )
			return 0;

		return (m_Data[m_Index++] << 24)
			 | (m_Data[m_Index++] << 16)
			 | (m_Data[m_Index++] <<  8)
			 |  m_Data[m_Index++];
	}
	
	public short ReadInt16()
	{
		if ( (m_Index + 2) > m_Size )
			return 0;

		return (short)((m_Data[m_Index++] << 8) | m_Data[m_Index++]);
	}
	
	public byte ReadByte()
	{
		if ( (m_Index + 1) > m_Size )
			return 0;

		return m_Data[m_Index++];
	}
	
	public long ReadUInt32()
	{
		if ( (m_Index + 4) > m_Size )
			return 0;

		return (long)((m_Data[m_Index++] << 24) | (m_Data[m_Index++] << 16) | (m_Data[m_Index++] << 8) | m_Data[m_Index++]);
	}
	
	public short ReadUInt16()
	{
		if ( (m_Index + 2) > m_Size )
			return 0;

		return (short)((m_Data[m_Index++] << 8) | m_Data[m_Index++]);
	}
	
	
	public boolean ReadBoolean()
	{
		if ( (m_Index + 1) > m_Size )
			return false;

		return ( m_Data[m_Index++] != 0 );
	}
	
	public String ReadUnicodeStringLE()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < m_Size && (c = (m_Data[m_Index++] | (m_Data[m_Index++] << 8))) != 0 )
			sb.append( (char)c );

		return sb.toString();
	}
	
	public String ReadUnicodeStringLESafe( int fixedLength )
	{
		int bound = m_Index + (fixedLength << 1);
		int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < bound && (c = (m_Data[m_Index++] | (m_Data[m_Index++] << 8))) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		m_Index = end;

		return sb.toString();
	}
	
	public String ReadUnicodeStringLESafe()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < m_Size && (c = (m_Data[m_Index++] | (m_Data[m_Index++] << 8))) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		return sb.toString();
	}

	public String ReadUnicodeStringSafe()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < m_Size && (c = ((m_Data[m_Index++] << 8) | m_Data[m_Index++])) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		return sb.toString();
	}

	public String ReadUnicodeString()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < m_Size && (c = ((m_Data[m_Index++] << 8) | m_Data[m_Index++])) != 0 )
			sb.append( (char)c );

		return sb.toString();
	}

	public boolean isSafeChar( int c )
	{
		return ( c >= 0x20 && c < 0xFFFE );
	}
	
	public String ReadUTF8StringSafe( int fixedLength )
	{
		if ( m_Index >= m_Size )
		{
			m_Index += fixedLength;
			return "";
		}

		int bound = m_Index + fixedLength;
		//int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		int count = 0;
		int index = m_Index;
		int start = m_Index;

		while ( index < bound && m_Data[index++] != 0 )
			++count;

		index = 0;

		byte[] buffer = new byte[count];
		int value = 0;

		while ( m_Index < bound && (value = m_Data[m_Index++]) != 0 )
			buffer[index++] = (byte)value;

		String s = new String(buffer);

		boolean isSafe = true;

		for ( int i = 0; isSafe && i < s.length(); ++i )
			isSafe = isSafeChar( (int) s.charAt(i) );

		m_Index = start + fixedLength;

		if ( isSafe )
			return s;

		StringBuilder sb = new StringBuilder( s.length() );

		for ( int i = 0; i < s.length(); ++i )
			if ( isSafeChar( (int) s.charAt(i) ) )
				sb.append( s.charAt(i) );

		return sb.toString();
	}
	
	public String ReadUTF8StringSafe()
	{
		if ( m_Index >= m_Size )
			return "";

		int count = 0;
		int index = m_Index;

		while ( index < m_Size && m_Data[index++] != 0 )
			++count;

		index = 0;

		byte[] buffer = new byte[count];
		int value = 0;

		while ( m_Index < m_Size && (value = m_Data[m_Index++]) != 0 )
			buffer[index++] = (byte)value;

		String s = new String( buffer );

		boolean isSafe = true;

		for ( int i = 0; isSafe && i < s.length(); ++i )
			isSafe = isSafeChar( (int) s.charAt(i) );

		if ( isSafe )
			return s;

		StringBuilder sb = new StringBuilder( s.length() );

		for ( int i = 0; i < s.length(); ++i )
		{
			if ( isSafeChar( (int) s.charAt(i) ) )
				sb.append( s.charAt(i) );
		}

		return sb.toString();
	}

	public String ReadUTF8String()
	{
		if ( m_Index >= m_Size )
			return "";

		int count = 0;
		int index = m_Index;

		while ( index < m_Size && m_Data[index++] != 0 )
			++count;

		index = 0;

		byte[] buffer = new byte[count];
		int value = 0;

		while ( m_Index < m_Size && (value = m_Data[m_Index++]) != 0 )
			buffer[index++] = (byte)value;

		return new String(buffer);
	}

	public String ReadString()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( m_Index < m_Size && (c = m_Data[m_Index++]) != 0 )
			sb.append( (char)c );

		return sb.toString();
	}

	public String ReadStringSafe()
	{
		StringBuilder sb = new StringBuilder();

		int c;

		while ( m_Index < m_Size && (c = m_Data[m_Index++]) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		return sb.toString();
	}

	public String ReadUnicodeStringSafe( int fixedLength )
	{
		int bound = m_Index + (fixedLength << 1);
		int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < bound && (c = ((m_Data[m_Index++] << 8) | m_Data[m_Index++])) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		m_Index = end;

		return sb.toString();
	}

	public String ReadUnicodeString( int fixedLength )
	{
		int bound = m_Index + (fixedLength << 1);
		int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		StringBuilder sb = new StringBuilder();

		int c;

		while ( (m_Index + 1) < bound && (c = ((m_Data[m_Index++] << 8) | m_Data[m_Index++])) != 0 )
			sb.append( (char)c );

		m_Index = end;

		return sb.toString();
	}

	public String ReadStringSafe( int fixedLength )
	{
		int bound = m_Index + fixedLength;
		int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		StringBuilder sb = new StringBuilder();

		int c;

		while ( m_Index < bound && (c = m_Data[m_Index++]) != 0 )
		{
			if ( isSafeChar( c ) )
				sb.append( (char)c );
		}

		m_Index = end;

		return sb.toString();
	}

	public String ReadString( int fixedLength )
	{
		int bound = m_Index + fixedLength;
		int end   = bound;

		if ( bound > m_Size )
			bound = m_Size;

		StringBuilder sb = new StringBuilder();

		int c;

		while ( m_Index < bound && (c = m_Data[m_Index++]) != 0 )
			sb.append( (char)c );

		m_Index = end;

		return sb.toString();
	}
	
}