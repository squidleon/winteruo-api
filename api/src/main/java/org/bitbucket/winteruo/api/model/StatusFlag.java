package org.bitbucket.winteruo.api.model;

public enum StatusFlag implements Coded {
	None(0), T2A(1), UOR(3), AOS(4), UOML(5); //, UOKR(6) --> not supported
	private int code;
	private StatusFlag(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
