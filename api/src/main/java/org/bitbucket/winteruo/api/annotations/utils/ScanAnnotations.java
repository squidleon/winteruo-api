package org.bitbucket.winteruo.api.annotations.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

import org.reflections.Reflections;


/**
 * Helper class for get annotations of certain type
 * @author tgiachi
 *
 */
@SuppressWarnings("unchecked")
public class ScanAnnotations {


	
	public static Set<Class<?>> getAnnotationFor(Class<?> classz, Object ... packages) throws Exception
	{
		try
		{
			Reflections reflections = new Reflections(packages);
			return reflections.getTypesAnnotatedWith((Class<? extends Annotation>) classz);
		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}
	}


	public static Set<Method> getAnnotatedMethodsFor(Class<?> classz, Object ... packages) throws Exception
	{
		Reflections reflex = new Reflections(packages);
		
		return reflex.getMethodsAnnotatedWith((Class<? extends Annotation>) classz);
	}
}
