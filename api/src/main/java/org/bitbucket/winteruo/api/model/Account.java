package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@XMLAlias
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int serialId;
	
	private String username;
	private String password;
	private Date createDate;
	private Date lastLoginDate;
	private boolean blocked = false;
	private AccountLevel level;
	
	
	private List<AccountCharacter> characters = new ArrayList<AccountCharacter>();


	
	public Account()
	{
		
	}
	
}
