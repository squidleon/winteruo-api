package org.bitbucket.winteruo.api.model;

import java.io.Serializable;


import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

@XMLAlias
public class Point3D extends Point2D implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private int z;
	
	public Point3D() {

	}
	
	public Point3D(int x, int y, int z) {
		super(x, y);
		setZ(z);
	}
	
	
	
}
