package org.bitbucket.winteruo.api.messages.enums;

public enum SkillLockState {

	UP(0x00),
	DOWN(0x01),
	LOCKED(0x02);
	
	private final int value;
	
	private SkillLockState(int value)
	{
		this.value = value;
		
	}
}
