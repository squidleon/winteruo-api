package org.bitbucket.winteruo.api.messages.enums;

public class FeatureFlags  {


	public static int None			= 0x0;
	public static int T2A			= 0x01;
	public static int UOR			= 0x02;
	public static int UOTD			= 0x04;
	public static int LBR			= 0x08;
	public static int AOS			= 0x10;
	public static int SixthCharacterSlot	= 0x20;
	public static int SE			= 0x40;
	public static int ML			= 0x80;
	public static int Unk1			= 0x100;
	public static int Unk2			= 0x200;
	public static int Unk3			= 0x400;
	public static int Unk4			= 0x800;
	public static int SeventhCharacterSlot	= 0x1000;
	public static int Unk5			= 0x2000;
	public static int Unk6			= 0x4000;
	public static int Unk7			= 0x8000;
	public static int SA				= 0x10000;
	public static int HS				= 0x20000;
	public static int Gothic			= 0x40000;
	public static int Rustic			= 0x80000;

	public static int ExpansionNone		= None;
	public static int ExpansionT2A		= T2A;
	public static int ExpansionUOR		= (int) (ExpansionT2A	| UOR);
	public static int ExpansionUOTD		= (int) (ExpansionUOR	| UOTD);
	public static int ExpansionLBR		= (int) (ExpansionUOTD	| LBR);
	public static int ExpansionAOS		= (int) (ExpansionLBR	| AOS	| Unk7);
	public static int ExpansionSE		= (int) (ExpansionAOS	| SE);
	public static int ExpansionML		= (int) (ExpansionSE	| ML	| Unk2);
	public static int ExpansionSA		= (int) (ExpansionML	| SA	| Gothic	| Rustic);
	public static int ExpansionHS		= ExpansionSA;






}


