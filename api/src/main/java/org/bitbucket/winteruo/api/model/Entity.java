package org.bitbucket.winteruo.api.model;

public interface Entity {
	int getModelId();

	int getSerialId();

	int getHue();

}
