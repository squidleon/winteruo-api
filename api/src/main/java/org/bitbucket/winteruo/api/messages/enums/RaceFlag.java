package org.bitbucket.winteruo.api.messages.enums;

import org.bitbucket.winteruo.api.model.Coded;

public enum RaceFlag implements Coded {
	Human(1), Elf(2), Gargoyle(3);
	private int code;
	private RaceFlag(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
