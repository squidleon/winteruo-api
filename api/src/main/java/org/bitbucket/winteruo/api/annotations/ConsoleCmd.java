package org.bitbucket.winteruo.api.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bitbucket.winteruo.api.network.ConsoleCommandLevel;

/**
 * Add this annotation for add console commands
 * @author tgiachi
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value=ElementType.METHOD)
public @interface ConsoleCmd {

	String cmd() default "";
	ConsoleCommandLevel level() default ConsoleCommandLevel.USER;
}
