package org.bitbucket.winteruo.api.utils;

import java.nio.ByteBuffer;

import org.apache.commons.lang.ArrayUtils;

public class PacketWriter {

	
	private ByteBuffer buffer;
	
	private static byte[] m_Buffer = new byte[4];
	
	public PacketWriter(int length) {
		buffer = ByteBuffer.allocate(length);
	
	}
	
	public void Write( Boolean value )
	{
		buffer.put( (byte)(value ? 1 : 0) );
	}
	
	public void Write( byte value )
	{
		buffer.put( value );
	}
	
	public void Write( short value )
	{
		m_Buffer[0] = (byte)(value >> 8);
		m_Buffer[1] = (byte) value;

		buffer.put( m_Buffer, 0, 2 );
	}
	
	public void Write( int value )
	{
		m_Buffer[0] = (byte)(value >> 24);
		m_Buffer[1] = (byte)(value >> 16);
		m_Buffer[2] = (byte)(value >>  8);
		m_Buffer[3] = (byte) value;

		buffer.put( m_Buffer, 0, 4 );
	}
	
	public void Write( byte[] buffer, int offset, int size )
	{
		this.buffer.put( buffer, offset, size );
	}
	
	public void Write(byte[] buffer)
	{
		this.buffer.put(buffer);
	}
	
	public void WriteReverse(byte[] buffer)
	{
		ArrayUtils.reverse(buffer);
		this.buffer.put(buffer);
	}
	
	
	public byte[] getBytes()
	{
		return this.buffer.array();
	}
}
