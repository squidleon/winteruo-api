package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

@XMLAlias
@Data
public class AccountRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private int lastSerialID = 0;
	
	private List<Account> accounts = new ArrayList<Account>();
	
	
	public void addAccount(Account account)
	{
		this.accounts.add(account);
	}
	
	public int nextId()
	{
		lastSerialID = lastSerialID+1;
		return this.lastSerialID;
	}
	
}
