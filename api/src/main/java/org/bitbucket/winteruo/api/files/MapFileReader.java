package org.bitbucket.winteruo.api.files;

import org.bitbucket.winteruo.api.model.MapLocation;
import org.bitbucket.winteruo.api.model.MapTile;

public interface MapFileReader extends GenericFileReader<MapLocation, MapTile> {}
