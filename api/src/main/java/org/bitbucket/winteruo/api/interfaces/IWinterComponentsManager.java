package org.bitbucket.winteruo.api.interfaces;

public interface IWinterComponentsManager {
	
	public void addComponent(IWinterComponent component) throws Exception;
	public void shutdown();

}
