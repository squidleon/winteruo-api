package org.bitbucket.winteruo.api.interfaces;

import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.listeners.INetworkServerListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;

public interface INetworkServer {
	
	public void start() throws Exception;
	public void stop() throws Exception;
	
	public boolean addNetworkListener(INetworkServerListener listener);
	
	public boolean register(byte code, IGameMessageListener listener);
	
	public AbstractMessage getMessageByCode(byte code);
	
	public void notifyGameMessagesListeners(byte code, AbstractMessage message, INetState netstate);
	
	
	public void printStatistics();
	
	

}
