package org.bitbucket.winteruo.api.model;

public class MapLocation extends Point2D {

	private static final long serialVersionUID = 1L;
	
	
	public MapLocation() {
	
	}
	
	public MapLocation(int x, int y) {
		setX(x);
		setY(y);
	}
	
}
