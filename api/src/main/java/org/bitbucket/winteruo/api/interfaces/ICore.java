package org.bitbucket.winteruo.api.interfaces;

import org.bitbucket.winteruo.api.data.MapsInfo;
import org.bitbucket.winteruo.api.data.ServerConfiguration;
import org.bitbucket.winteruo.api.listeners.ICoreListener;
import org.bitbucket.winteruo.api.logger.IWinterLogger;
import org.bitbucket.winteruo.api.model.ServerInfo;

/**
 * Implements Core class
 * @author tommasogiachi
 *
 */
public interface ICore {
	/**
	 * Start new server
	 * @return
	 * @throws Exception
	 */
	public boolean startServer() throws Exception;
	/**
	 * Stop server and close connections
	 * @return
	 * @throws Exception
	 */
	public boolean stopServer() throws Exception;
	/**
	 * Retrive server configuration
	 * @return
	 */
	public ServerConfiguration getServerConfiguration();
	/**
	 * Get Winter components manager
	 * @return
	 */
	public IWinterComponentsManager getWinterComponentsManager();
	/**
	 * Retrive new logger instance
	 * @param classz
	 * @return
	 */
	public IWinterLogger getLogger(Class<?> classz);
	/**
	 * Add to Core new listener
	 * @param listener
	 * @return
	 */
	public boolean addCoreListener(ICoreListener listener);
	/**
	 * Retrive configuration manager instance
	 */
	public IConfigurationsManager getConfigurationsManager();
	/**
	 * Retrive network server
	 * @return
	 */
	public INetworkServer getNetworkServer();
	/**
	 * Retrive authtentication manager
	 * @return
	 */
	public IAuthenticationManager getAuthenticationManager();
	/**
	 * Return current instance of FileManager
	 * @return
	 */
	public IFileManager getFileManager();
	
	public MapsInfo getMapsInfo();
	/**
	 * Get server connection's info
	 * @return
	 */
	public ServerInfo getServerInfo();
	

}
