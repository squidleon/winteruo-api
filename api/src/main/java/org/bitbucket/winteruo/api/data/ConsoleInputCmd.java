package org.bitbucket.winteruo.api.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
/**
 * Object for input command from console of ConsoleComponent
 * 
 * @author tgiachi
 *
 */
public class ConsoleInputCmd implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String command;
	private List<String> args = new ArrayList<String>();
	
	
	public ConsoleInputCmd(String command)
	{
		this.command = command;

	}

	
	public void addArg(String arg)
	{
		args.add(arg);
	}
}
