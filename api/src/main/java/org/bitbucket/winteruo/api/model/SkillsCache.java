package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

@XMLAlias
@Data
public class SkillsCache implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<SkillGame> skills = new ArrayList<SkillGame>();
	
	
	public void addSkillGame(SkillGame skill)
	{
		this.skills.add(skill);
	}
	

}
