package org.bitbucket.winteruo.api.messages.enums;

public enum SeasonFlag {

	SPRING(0),
	SUMMER(1),
	FALL(2),
	WINTER(3),
	DESOLATION(4);

	private final int value;

	private SeasonFlag(int value) {
		this.value = value;
	}


}
