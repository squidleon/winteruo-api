package org.bitbucket.winteruo.api.data;

import java.io.Serializable;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

import lombok.Data;


/**
 * Class where stored server configuration, normally filled on Boot
 * @author tgiachi
 *
 */
@Data
@XMLAlias
public class ServerConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private String serverName;
	private int serverPort;
	private String serverAddress;
	private String uoDirectory;

	private boolean packetLogging = false;
	private boolean debug = false;
	
	public ServerConfiguration(String serverName, int serverPort, String serverAddress, String uoDirectory)
	{
		this.serverName= serverName;
		this.serverPort= serverPort;
		this.serverAddress = serverAddress;
		this.uoDirectory = uoDirectory;
	}
	
	public ServerConfiguration()
	{
		
	}
}
