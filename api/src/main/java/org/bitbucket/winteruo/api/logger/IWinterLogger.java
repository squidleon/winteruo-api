package org.bitbucket.winteruo.api.logger;

import org.apache.log4j.Level;

/**
 * Implement logger interface
 * @author tgiachi
 *
 */
public interface IWinterLogger {

	void info(String text, Object ... args);
	
	void warn(String text, Object  ... args);
	
	void debug(String text, Object  ... args);
	
	void error(String text, Object ... args);
	
	void exception(Exception ex, String text, Object ... args);
	
	void log(Level level, String text, Object ... args);
	
	IWinterLogger newInstance(Class<?> classz);
	
}
