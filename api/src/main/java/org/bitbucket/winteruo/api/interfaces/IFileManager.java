package org.bitbucket.winteruo.api.interfaces;

import java.util.List;

import org.bitbucket.winteruo.api.model.MapFileInfo;
import org.bitbucket.winteruo.api.model.MapLocation;
import org.bitbucket.winteruo.api.model.MapTile;
import org.bitbucket.winteruo.api.model.SkillInfo;

public interface IFileManager {

	public abstract void prepareMap(MapFileInfo map);

	public abstract void prepareMap(MapFileInfo... maps);
	
	public abstract MapFileInfo getMapFileInfoById(int id);

	public abstract MapTile getMapTileByIndex(int idx, MapLocation location)
			throws Exception;

	public abstract MapTile getMapTileByIndex(int idx, int x, int y)
			throws Exception;
	
	public abstract  SkillInfo getSkillInfoById(int id);
	
	public abstract List<SkillInfo> getSkillsInfo();

}