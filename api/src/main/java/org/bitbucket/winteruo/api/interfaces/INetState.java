package org.bitbucket.winteruo.api.interfaces;

import java.util.List;

import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.VersionInfo;


/**
 * Interface to server
 * @author tgiachi
 *
 */
public interface INetState {

	public boolean sendMessage(AbstractMessage message);
	public boolean sendMessage(List<AbstractMessage> messages);
	
	public void broadcastMessage(AbstractMessage message);
	public void broadcastMessage(List<AbstractMessage> messages);
	
	public int getSeed();
	public int getAuthID();
	public void setAuthID(int value);
	
	public AccountCharacter getCurrentCharacter();
	public void setCurrentCharacter(AccountCharacter character);
	
	public boolean isFirstPacketSend();
	
	public void setAccount(Account account);
	public Account getAccount();
	
	public boolean isSeeded();
	
	public VersionInfo getVersionInfo();
	public void setVersionInfo(VersionInfo versionInfo);
	
}
