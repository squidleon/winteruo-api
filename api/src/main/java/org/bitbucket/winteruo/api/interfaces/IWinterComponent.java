package org.bitbucket.winteruo.api.interfaces;

import org.bitbucket.winteruo.api.logger.IWinterLogger;

/**
 * Interface for Winter Component
 * @author tgiachi
 *
 */
public interface IWinterComponent {

	void setCore(ICore core);
	
	void init();
	void start();
	void stop();
	void setLogger(IWinterLogger logger);
	
	
}
