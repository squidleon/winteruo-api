package org.bitbucket.winteruo.api.model;

public interface ItemVisitor {
	void visit(Item item);
	void visit(Container item);
}
