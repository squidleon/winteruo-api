package org.bitbucket.winteruo.api.model;

public interface JUoEntity {
	int getModelId();

	int getSerialId();
	void setSerialId(int value);

	int getHue();
}
