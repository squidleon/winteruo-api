package org.bitbucket.winteruo.api.files;

/**
 * Index file entry.
 */
public interface IdxFileEntry {
	IdxFileEntry index(int index);

	int getIndex();

	int getStart();

	int getLength();
}
