package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.net.InetAddress;

import lombok.Data;

@Data
public class ServerInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String serverName;
	private int port;
	private InetAddress address;
	
}
