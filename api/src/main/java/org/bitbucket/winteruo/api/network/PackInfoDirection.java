package org.bitbucket.winteruo.api.network;

public enum PackInfoDirection {

	FROM_CLIENT,
	FROM_SERVER,
	BOTH
}
