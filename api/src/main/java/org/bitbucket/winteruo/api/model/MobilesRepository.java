package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.HashMap;

import lombok.Data;

@Data
public class MobilesRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	private HashMap<Integer, UOMobile> mobiles = new HashMap<Integer, UOMobile>();
	
	
	
}
