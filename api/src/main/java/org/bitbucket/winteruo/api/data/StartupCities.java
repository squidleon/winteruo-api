package org.bitbucket.winteruo.api.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.model.City;

import lombok.Data;

@Data
@XMLAlias
public class StartupCities implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private List<City> cities = new ArrayList<City>();



	public void addCity(City city)
	{
		this.cities.add(city);
	}
	
}
