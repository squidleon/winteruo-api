package org.bitbucket.winteruo.api.model;

public enum AccountLevel {

	NORMAL, GM, ADMINISTRATOR
}
