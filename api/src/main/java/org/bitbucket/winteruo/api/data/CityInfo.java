package org.bitbucket.winteruo.api.data;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.model.MapFileInfo;
import org.bitbucket.winteruo.api.model.Point3D;


@XMLAlias
@Data
@AllArgsConstructor
public class CityInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String building;
	private String city;
	private int description;
	private Point3D location;
	private int mapId;
	
	public CityInfo()
	{
		
	}
	
}
