package org.bitbucket.winteruo.api.interfaces;

import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.api.model.AccountCharacter;

public interface IAuthenticationManager {
	
	public Account authenticate(String username, String password);
	public boolean addCharacter(Account clientAccount, AccountCharacter newCharacter);
	public AccountCharacter getCharacterByName(Account account, String characterName);
	public Account reloadAccount(int serialId);

}
