package org.bitbucket.winteruo.api.messages.enums;

public class CharacterListFlags {

	public static int	None			= 0x00;
	public static int	Unk1			= 0x01;
	public static int	Unk2			= 0x02;
	public static int	OneCharacterSlot	= 0x04;
	public static int	ContextMenus		= 0x08;
	public static int	SlotLimit		= 0x10;
	public static int	AOS			= 0x20;
	public static int	SixthCharacterSlot	= 0x40;
	public static int	SE			= 0x80;
	public static int	ML			= 0x100;
	public static int	Unk4			= 0x200;
	public static int	Unk5			= 0x400;
	public static int	Unk6			= 0x800;
	public static int	SeventhCharacterSlot	= 0x1000;
	public static int	Unk7			= 0x2000;

	public static int			ExpansionNone		= ContextMenus; //
	public static int ExpansionT2A		= ContextMenus; //
	public static int			ExpansionUOR		= ContextMenus; // None
	public static int			ExpansionUOTD		= ContextMenus; //
	public static int			ExpansionLBR		= ContextMenus; //
	public static int			ExpansionAOS		= (int) (ContextMenus	| AOS);
	public static int			ExpansionSE		= (int) (ExpansionAOS	| SE);
	public static int			ExpansionML		= (int) (ExpansionSE	| ML);
	public static int			ExpansionSA		= ExpansionML;
	public static int			ExpansionHS		= ExpansionSA;

}
