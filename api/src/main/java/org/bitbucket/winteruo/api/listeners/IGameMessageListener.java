package org.bitbucket.winteruo.api.listeners;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.messages.AbstractMessage;

public interface IGameMessageListener {

	
	void onMessageReceived(AbstractMessage message, INetState netState, ICore core);
	
}
