package org.bitbucket.winteruo.api.files;

/**
 * Skills data file entry.
 */
public interface SkillsFileEntry {
	String getSkillName();

	boolean isHasButton();
}