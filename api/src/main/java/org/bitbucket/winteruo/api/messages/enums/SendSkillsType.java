package org.bitbucket.winteruo.api.messages.enums;

public enum SendSkillsType {
	
	FULL_LIST(0x00),
	SINGLE_SKILL_UPDATE(0xFF),
	FULL_LIST_WITH_CAP(0x02),
	SINGLE_SKILL_UPDATE_WITH_CAP(0xDF);
	
	private final int value;

	private SendSkillsType(int value) {

		this.value = value;
	}

}
