package org.bitbucket.winteruo.api.files;


/**
 * Contract for a class capable of encoding
 * {@link IdxFileEntryImpl}s starting from byte
 * arrays.
 */
public interface IdxFileEntryEncoder extends GenericFileEntryEncoder<IdxFileEntry> {}
