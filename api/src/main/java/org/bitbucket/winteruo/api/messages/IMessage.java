package org.bitbucket.winteruo.api.messages;

import org.bitbucket.winteruo.api.utils.PacketReader;

/**
 * Interface UO message
 * @author tgiachi
 *
 */
public interface IMessage {

	/**
	 * Parse incoming message
	 * @param buffer
	 */
	public abstract void parse(PacketReader reader);
	/**
	 * Return bytes array to send
	 * @return
	 */
	public abstract byte[] getBytes();
	/**
	 * get length of message
	 * @return
	 */
	public int getLength();
	/**
	 * Return message Code
	 * @return
	 */
	public byte getCode();
	
	public boolean isCompressed();
}
