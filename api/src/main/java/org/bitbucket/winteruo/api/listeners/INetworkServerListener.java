package org.bitbucket.winteruo.api.listeners;

import org.bitbucket.winteruo.api.messages.AbstractMessage;

public interface INetworkServerListener {

	
	public void onByteReceived(String sessionId, byte[] buffer);
	public void onByteSended(String sessionId, byte[] buffer);
	
	public void onMessageReceived(String sessionId, AbstractMessage message);
	public void onMessageSended(String sessionId, AbstractMessage message);
}
