package org.bitbucket.winteruo.api.model;


public class UOItem implements Item {
	private int serialId;
	private int modelId;
	private int hue;

	public UOItem(int serialId, int modelId, int hue) {
		super();
		this.serialId = serialId;
		this.modelId = modelId;
		this.hue = hue;
	}


	public int getSerialId() {
		return serialId;
	}

	public void setSerialId(int value) {
		this.serialId = value;		
	}


	public int getModelId() {
		return modelId;
	}


	public int getHue() {
		return hue;
	}


	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + serialId;
		return result;
	}


	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UOItem other = (UOItem) obj;
		if (serialId != other.serialId)
			return false;
		return true;
	}


	public String toString() {
		return "UOItem [serialId=" + serialId + ", modelId=" + modelId + "]";
	}


	public void accept(ItemVisitor itemManager) {
		itemManager.visit(this);
	}
}
