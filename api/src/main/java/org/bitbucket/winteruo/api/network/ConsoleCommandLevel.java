package org.bitbucket.winteruo.api.network;

public enum ConsoleCommandLevel {

	ADMINISTRATOR,
	GM,
	USER
}
