package org.bitbucket.winteruo.api.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;


@Data
@XMLAlias
@AllArgsConstructor
public class SkillGame implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private SkillInfo baseSkill;
	private int value;
	private int unmodifiedValue;
	private int cap;
	private SkillLockFlag lockFlag = SkillLockFlag.Up;
	
	
	
	
	public SkillGame()
	{
		
	}
	
	


}
