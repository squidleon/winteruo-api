package org.bitbucket.winteruo.api.messages;

import java.io.Serializable;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.utils.PacketReader;

public class AbstractMessage implements IMessage, Serializable {


	private static final long serialVersionUID = 1L;


	private byte code;


	private PacketInfo packetInfoAnnotation;

	public AbstractMessage(byte code) {

		this.code = code;
		packetInfoAnnotation = this.getClass().getAnnotation(PacketInfo.class);
		
	}

	public void parse(PacketReader reader) {
		
	}

	public byte[] getBytes() {

		return null;
	}

	public int getLength() {

		return 0;
	}

	public byte getCode() {

		return code;
	}


	public boolean isCompressed() {

		
		if (packetInfoAnnotation != null)
			return packetInfoAnnotation.isCompress();
		else
			return false;
	}


	@Override
	public String toString() {	
		return String.format("[%s]", this.getClass().getSimpleName());
	}


	//	private String formatBytes(byte[] buffer)
	//	{
	//		String res = "";
	//		for (int i=0;i<buffer.length;i++)
	//		{
	//			res += String.format("0x%02x,", buffer[i]);
	//		}
	//		
	//		return res;
	//
	//	}
}
