package org.bitbucket.winteruo.api.files;

public interface GenericFileEntryEncoder<T> {
	public T encode(byte[] contents);
}
