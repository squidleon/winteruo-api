package org.bitbucket.winteruo.api.listeners;

import org.bitbucket.winteruo.api.data.ServerConfiguration;


/***
 * Interface for Core listener
 * @author tgiachi
 *
 */
public interface ICoreListener {
	
	void onServerConfiguration(ServerConfiguration configuration);
	void onStart();
	void onInit();
	void onStartNetworkServer();
	void onStopNetworkServer();
}
