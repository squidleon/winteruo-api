package org.bitbucket.winteruo.api.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.model.MapFileInfo;

@XMLAlias
@Data
public class MapsInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<MapFileInfo> maps = new ArrayList<MapFileInfo>();
	private List<CityInfo> citiesInfo = new ArrayList<CityInfo>();
	
	
	public void addMap(MapFileInfo map)
	{
		maps.add(map);
	}
	
	public void addCityInfo(CityInfo cityInfo)
	{
		citiesInfo.add(cityInfo);
	}
	

	
}
