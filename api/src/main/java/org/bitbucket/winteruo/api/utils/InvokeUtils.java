package org.bitbucket.winteruo.api.utils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;


/**
 * Class for method invoking
 * @author tgiachi
 *
 */
public class InvokeUtils {
	/***
	 * Notify listeners
	 * @param methodName
	 * @param paramsType
	 * @param params
	 */
	public static void invokeListeners(List<?> listeners, String methodName, Class<?>[] paramsType, Object ... params)
	{
		try
		{
			for (Object listener: listeners)
			{
				Method method = listener.getClass().getDeclaredMethod(methodName, paramsType);
				method.setAccessible(true);
				method.invoke(listener, params);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public static void invokeListeners(List<?> listeners, String methodName)
	{
		invokeListeners(listeners, methodName, null, null);
	}
}

