package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.data.CityInfo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@XMLAlias
public class MapFileInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private int mapId;
	private int size;
	private String mapName;
	
	private List<CityInfo> cityInfo = new ArrayList<CityInfo>();
	

	public MapFileInfo()
	{
		
	}
	
}
