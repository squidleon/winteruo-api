package org.bitbucket.winteruo.api.messages.enums;

public enum GetPlayerStatusType {


	GOD_CLIENT(0x00),
	BASIC_STATUS(0x04),
	REQUEST_SKILLS(0x05);

	private final int value;

	private GetPlayerStatusType(int value) {

		this.value = value;
	}

	public static GetPlayerStatusType getType(byte value)
	{
		for (GetPlayerStatusType ty: values())
		{
			if (ty.value == value)
				return ty;
		}
		return null;
	}
}
