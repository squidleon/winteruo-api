package org.bitbucket.winteruo.api.model;

public enum SkillLockFlag implements Coded {
	Up(0), Down(1), Locked(2);
	
	private int code;

	private SkillLockFlag(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
