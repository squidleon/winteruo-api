package org.bitbucket.winteruo.api.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
/**
 * Annotation for add to SerializerManager XML alias.
 * XStream by default save to root element full class like <i>org.bitbucket.winteruo.api.data.MyData</i>
 * adding this annotation save on <b>Class.getSimpleName</b>. Of course the alias must be unique!!
 * @author tgiachi
 *
 */
public @interface XMLAlias {
	String alias() default  "";
}
