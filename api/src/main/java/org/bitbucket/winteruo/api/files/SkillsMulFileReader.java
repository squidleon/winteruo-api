package org.bitbucket.winteruo.api.files;

/**
 * Skills data file reader.
 */
public interface SkillsMulFileReader extends IndexedFileReader<SkillsFileEntry> {}
