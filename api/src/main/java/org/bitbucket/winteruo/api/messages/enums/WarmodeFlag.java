package org.bitbucket.winteruo.api.messages.enums;

public enum WarmodeFlag {

	PEACE(0), WAR(1);


	private final int value;

	private WarmodeFlag(int value) {
		this.value = value;
	}
}
