package org.bitbucket.winteruo.api.messages.enums;

public enum Direction {

	NORTH(0x00),
	NORTH_EAST(0x01),
	EAST(0x02),
	SOUTH_EAST(0x03),
	SOUTH(0x04),
	SOUTH_WEST(0x05),
	WEST(0x06),
	NORTH_WEST(0x07);


	private final int value;

	private Direction(int value) {

		this.value = value;
	}
}
