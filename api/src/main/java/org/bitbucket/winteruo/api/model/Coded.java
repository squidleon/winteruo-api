package org.bitbucket.winteruo.api.model;

/**
 * Anything identifiable by an integer code.
 */
public interface Coded {
	/**
	 * @return the identifying code
	 */
	int getCode();
}
