package org.bitbucket.winteruo.api.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.messages.enums.CharacterStatus;
import org.bitbucket.winteruo.api.messages.enums.Direction;
import org.bitbucket.winteruo.api.messages.enums.Layer;
import org.bitbucket.winteruo.api.messages.enums.Notoriety;
import org.bitbucket.winteruo.api.messages.enums.RaceFlag;
import org.bitbucket.winteruo.api.messages.enums.SexType;


public class UOMobile implements Mobile {
	private int serialId;
	/**
	 * The body type.
	 */
	@Setter
	private int modelId = 0x190;
	@Setter
	private String name;
	@Setter
	private int currentHitPoints;
	@Setter
	private int maxHitPoints;
	@Setter
	private boolean nameChangeFlag;
	@Setter
	private StatusFlag statusFlag;
	@Setter
	private SexType sexRace;
	@Setter
	private int strength;
	@Setter
	private int dexterity;
	@Setter
	private int intelligence;
	@Setter
	private int currentStamina;
	@Setter
	private int maxStamina;
	@Setter
	private int currentMana;
	@Setter
	private int maxMana;
	@Setter
	private int goldInPack;
	@Setter
	private int armorRating;
	@Setter
	private int weight;



	@Setter
	private short skinColor;
	@Setter
	private short hairStyle;
	@Setter
	private short hairColor;

	@Setter
	private short facialHair;
	@Setter
	private short faciaHairColor;
	
	@Setter
	private short shirtColor;
	@Setter
	private short pantsColor;

	// statusFlag >= StatusFlag.UOML
	private int maxWeight;
	private RaceFlag raceFlag;

	private String title = "The Great";

	//	private Set<Skill> skills = new HashSet<Skill>( Arrays.asList(new Skill(Skills.Alchemy, 85, 80, 100, SkillLockFlag.Up),
	//					new Skill(Skills.Magery, 95, 90, 100, SkillLockFlag.Up),
	//					new Skill(Skills.Inscription, 95, 90, 100, SkillLockFlag.Up),
	//					new Skill(Skills.EvaluateIntelligence, 100, 100, 100, SkillLockFlag.Down),
	//					new Skill(Skills.ItemIdentify, 75, 70, 100, SkillLockFlag.Locked)) );


	private Set<SkillGame> skills = new HashSet<SkillGame>();

	private Map<Layer, Item> items;
	private int hue = 0x83EA;
	@Setter
	private int x = 0x02E8;
	@Setter
	private int y = 0x0877;
	@Setter
	private int z = 5;
	private Direction direction = Direction.SOUTH_EAST;
	private boolean running;
	private Notoriety notoriety = Notoriety.INNOCENT;
	private CharacterStatus characterStatus = CharacterStatus.Normal;

	public UOMobile(int playerSerial, String playerName, int currentHitPoints,
			int maxHitPoints, boolean nameChangeFlag, StatusFlag statusFlag,
			SexType sexRace, int strength, int dexterity, int intelligence,
			int currentStamina, int maxStamina, int currentMana, int maxMana,
			int goldInPack, int armorRating, int weight, int maxWeight,
			RaceFlag raceFlag) {
		super();
		this.serialId = playerSerial;
		this.name = playerName;
		this.currentHitPoints = currentHitPoints;
		this.maxHitPoints = maxHitPoints;
		this.nameChangeFlag = nameChangeFlag;
		this.statusFlag = statusFlag;
		this.sexRace = sexRace;
		this.strength = strength;
		this.dexterity = dexterity;
		this.intelligence = intelligence;
		this.currentStamina = currentStamina;
		this.maxStamina = maxStamina;
		this.currentMana = currentMana;
		this.maxMana = maxMana;
		this.goldInPack = goldInPack;
		this.armorRating = armorRating;
		this.weight = weight;
		this.maxWeight = maxWeight;
		this.raceFlag = raceFlag;

		items = new HashMap<Layer, Item>();
	}


	public UOMobile() {
		// TODO Auto-generated constructor stub
	}


	public int getModelId() {
		return modelId;
	}


	public int getSerialId() {
		return serialId;
	}


	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	public int getZ() {
		return z;
	}


	public void setZ(int z) {
		this.z = z;
	}


	public Direction getDirection() {
		return direction;
	}


	public void setDirection(Direction direction) {
		this.direction = direction;
	}


	public boolean isRunning() {
		return running;
	}


	public void setRunning(boolean running) {
		this.running = running;
	}


	public String getName() {
		return name;
	}


	public int getCurrentHitPoints() {
		return currentHitPoints;
	}


	public int getMaxHitPoints() {
		return maxHitPoints;
	}


	public boolean isNameChangeFlag() {
		return nameChangeFlag;
	}


	public StatusFlag getStatusFlag() {
		return statusFlag;
	}


	public SexType getSexRace() {
		return sexRace;
	}


	public int getStrength() {
		return strength;
	}


	public int getDexterity() {
		return dexterity;
	}


	public int getIntelligence() {
		return intelligence;
	}


	public int getCurrentStamina() {
		return currentStamina;
	}


	public int getMaxStamina() {
		return maxStamina;
	}


	public int getCurrentMana() {
		return currentMana;
	}


	public int getMaxMana() {
		return maxMana;
	}


	public int getGoldInPack() {
		return goldInPack;
	}


	public int getArmorRating() {
		return armorRating;
	}


	public int getWeight() {
		return weight;
	}


	public int getMaxWeight() {
		return maxWeight;
	}


	public RaceFlag getRaceFlag() {
		return raceFlag;
	}


	public String getTitle() {
		return title;
	}


	public Set<SkillGame> getSkills() {
		return skills;
	}


	public Map<Layer, Item> getItems() {
		return items;
	}


	public int getHue() {
		return hue;
	}


	public Notoriety getNotoriety() {
		return notoriety;
	}


	public CharacterStatus getCharacterStatus() {
		return characterStatus;
	}

	/**
	 * Updates this mobile's position according to their {@link #direction}.
	 */

	public void moveForward() {
		switch (direction) {
		case NORTH:
			--y;
			break;
		case NORTH_EAST:
			++x;
			--y;
			break;
		case EAST:
			++x;
			break;
		case SOUTH_EAST:
			++x;
			++y;
			break;
		case SOUTH:
			++y;
			break;
		case SOUTH_WEST:
			--x;
			++y;
			break;
		case WEST:
			--x;
			break;
		case NORTH_WEST:
			--x;
			--y;
			break;
		}
	}


	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + serialId;
		return result;
	}


	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UOMobile other = (UOMobile) obj;
		if (serialId != other.serialId)
			return false;
		return true;
	}

	public String toString() {
		return name;
	}


	public String getPrefixNameSuffix() {
		return " \t" + getName() + "\t ";
	}


	public byte getDirectionWithRunningInfo() {
		return (byte) (direction.ordinal() | (isRunning()? 0x80 : 0));
	}


	public void setItemOnLayer(Layer layer, Item item) {
		items.put(layer, item);
	}


	public Item getItemByLayer(Layer layer) {
		return items.get(layer);
	}


	public boolean removeItem(Item item) {
		return items.remove(getLayer(item)) != null;
	}


	public Layer getLayer(Item item) {
		for (Layer layer : Collections.unmodifiableSet(items.keySet())) {
			if (getItemByLayer(layer).equals(item)) {
				return layer;
			}
		}
		return null;
	}


	public void emptyLayer(Layer layer) {
		removeItem(getItemByLayer(layer));
	}


	public void setCharacterStatus(CharacterStatus characterStatus) {
		this.characterStatus = characterStatus;		
	}


	public short getSkinColor() {

		return skinColor;
	}


	public short getHairStyle() {
		// TODO Auto-generated method stub
		return hairStyle;
	}


	public short getHairColor() {
		// TODO Auto-generated method stub
		return hairColor;
	}


	public short getFacialHair() {
		// TODO Auto-generated method stub
		return facialHair;
	}


	public short getFaciaHairColor() {
		// TODO Auto-generated method stub
		return faciaHairColor;
	}


	public short getShirtColor() {
		// TODO Auto-generated method stub
		return shirtColor;
	}


	public short getPantsColor() {
		// TODO Auto-generated method stub
		return pantsColor;
	}

	public void setSerialId(int value) {

		this.serialId = value;	
	}



}
