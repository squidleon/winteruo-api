package org.bitbucket.winteruo.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.data.CityInfo;
import org.bitbucket.winteruo.api.messages.enums.Direction;
import org.bitbucket.winteruo.api.messages.enums.SexType;

@XMLAlias
@Data
@AllArgsConstructor
public class AccountCharacter implements Serializable {

	
	private static final long serialVersionUID = 1L;

	
	private String name;
	private String password;
	
	
	private int mobileID;
	
	private int startupLocationId;
	

	
	

	
	
	
	
	
	
	public AccountCharacter()
	{
		
	}
	
}
