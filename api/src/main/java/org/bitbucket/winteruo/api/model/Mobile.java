package org.bitbucket.winteruo.api.model;

import java.util.Map;
import java.util.Set;

import org.bitbucket.winteruo.api.messages.enums.CharacterStatus;
import org.bitbucket.winteruo.api.messages.enums.Direction;
import org.bitbucket.winteruo.api.messages.enums.Layer;
import org.bitbucket.winteruo.api.messages.enums.Notoriety;
import org.bitbucket.winteruo.api.messages.enums.RaceFlag;
import org.bitbucket.winteruo.api.messages.enums.SexType;

public interface Mobile extends JUoEntity {
	void setZ(int z);

	Direction getDirection();

	void setDirection(Direction direction);

	boolean isRunning();

	void setRunning(boolean running);

	String getName();

	int getCurrentHitPoints();

	int getMaxHitPoints();

	boolean isNameChangeFlag();

	StatusFlag getStatusFlag();

	SexType getSexRace();

	int getStrength();

	int getDexterity();

	int getIntelligence();

	int getCurrentStamina();

	int getMaxStamina();

	int getCurrentMana();

	int getMaxMana();

	int getGoldInPack();

	int getArmorRating();

	int getWeight();

	int getMaxWeight();
	
	
	short getSkinColor();
	
	short getHairStyle();
	
	short getHairColor();
	

	short getFacialHair();
	
	short getFaciaHairColor();
	
	short getShirtColor();
	
	short getPantsColor();
	
	

	RaceFlag getRaceFlag();

	String getTitle();

	Set<SkillGame> getSkills();

	Map<Layer, Item> getItems();

	Notoriety getNotoriety();

	CharacterStatus getCharacterStatus();

	/**
	 * Updates this mobile's position according to their {@link Direction}.
	 */
	void moveForward();

	String getPrefixNameSuffix();

	byte getDirectionWithRunningInfo();

	void setItemOnLayer(Layer layer, Item item);

	Item getItemByLayer(Layer layer);

	boolean removeItem(Item item);

	Layer getLayer(Item item);

	void emptyLayer(Layer layer);
	
	void setCharacterStatus(CharacterStatus characterStatus);
}