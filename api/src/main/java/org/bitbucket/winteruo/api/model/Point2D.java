package org.bitbucket.winteruo.api.model;

import java.io.Serializable;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

import lombok.AllArgsConstructor;
import lombok.Data;


@XMLAlias
@Data
@AllArgsConstructor
public class Point2D implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int x;
	private int y;

	public Point2D() {

	}

}
