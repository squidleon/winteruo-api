package org.bitbucket.winteruo.api.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VersionInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private int clientMaj;
	private int clientMin;
	private int clientRev;
	private int clientPat;
	
}
