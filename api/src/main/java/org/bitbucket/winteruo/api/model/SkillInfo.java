package org.bitbucket.winteruo.api.model;

import java.io.Serializable;

import org.bitbucket.winteruo.api.annotations.XMLAlias;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@XMLAlias
public class SkillInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private int skillId;
	private String name;
	
	public SkillInfo() {
	
	}
}
